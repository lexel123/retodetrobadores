import { RetroDeTrobadoresPage } from './app.po';

describe('retro-de-trobadores App', () => {
  let page: RetroDeTrobadoresPage;

  beforeEach(() => {
    page = new RetroDeTrobadoresPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
