import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  usuarios = [{
    name: 'Jose',
    apellido: 'Jaen',
    usuario:'jjaen',
    correo:'jjaen@revosoft.com.pa',
    rol: 'Usuario',
    departamento:'Produccion'
  },{
    name: 'Joaquin',
    apellido: 'Mendieta',
    usuario:'jmendieta',
    correo:'jmendieta@argos.com.co',
    rol: 'Jefe',
    departamento:'Produccion'
  },{
    name: 'Luisa',
    apellido: 'Serrano',
    usuario:'lserrano',
    correo:'lserrano@argos.com.co',
    rol: 'Jefe',
    departamento:'Produccion'
  },{
    name: 'Aljadis',
    apellido: 'Sanchez',
    usuario:'asanchez',
    correo:'asanchez@argos.co',
    rol: 'Usuario',
    departamento:'Produccion'
  },{
    name: 'Jose',
    apellido: 'Cruz Negro',
    usuario:'jjzcru',
    correo:'jjzcru@revosoft.com.pa',
    rol: 'Usuario',
    departamento:'Produccion'
  },{
    name: 'Elio',
    apellido: 'Rudas',
    usuario:'erudas',
    correo:'elio.rudas@argos.co',
    rol: 'Usuario',
    departamento:'Produccion'
  },{
    name: 'jose',
    apellido: 'Andrion',
    usuario:'Jandrion',
    correo:'operadores@argos.co',
    rol: 'Usuario',
    departamento:'Produccion'
  }

]
  constructor() { }

  ngOnInit() {
  }
  add(){


  }
  edit(){

  }
  remove(index){
    this.usuarios.splice(index,1);
  }

}
