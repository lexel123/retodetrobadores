import { LoginComponent } from './login/login.component';
import { UsersComponent } from './users/users.component';

export const AppRoutes: any = [
    {path: "", component: LoginComponent},
    {path:"users",component:UsersComponent}
];
